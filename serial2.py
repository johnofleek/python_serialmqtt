#!/usr/bin/python

import atexit
import time
import sys
import os
import argparse


# python serial2.py --up "test" --down "test" --port "COM19"

# This shows a simple example of an MQTT subscriber.
import logging.handlers
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

FORMAT = '%(asctime)s : %(name)s : (%(threadName)-10s) :%(levelname)s : %(message)s'
logging.basicConfig(format=FORMAT)  ## logger writes to console

logger.info('serial2.py started')


#import context  # Ensures paho is in PYTHONPATH
import paho.mqtt.client as mqtt

import serial    # serial port


def exitHandler():
    global mqttc
    mqttc.loop_start()
    logger.info("mqtt threads stopped")

mqttConnected = False

payload = "No special SIM contracts needed"

def on_connect(mqttc, obj, flags, rc):
    global mqttConnected
    global args
    
    mqttConnected = True
    logger.info("connect: " + str(rc))
    
    mqttc.subscribe(args.down, 0)   # so that if there is a disconnect the subscriptions are restored
    
def on_disconnect(mqttc, obj, flags, rc):
    global mqttConnected

# this is in another thread
def on_message(mqttc, obj, msg):
    logger.info("msgUp:" + msg.topic + " " + str(msg.qos) + " " + str(msg.payload))
    global ser
    ser.write(msg.payload)


upcount = 0

def on_publish(mqttc, obj, mid):
    global upcount
    logger.info("publish: " + str(mid))
    upcount+=1


def on_subscribe(mqttc, obj, mid, granted_qos):
    logger.info("Subscribed: " + str(mid) + " qos: " + str(granted_qos))


def on_log(mqttc, obj, level, string):
    logger.info(string)


# command line
parser = argparse.ArgumentParser(description='set the uplink and downlink topics')
parser.add_argument('--up', required=True ,help='Enter mqtt uplink topic')
parser.add_argument('--down',required=True , help='Enter mqtt downlink topic')
parser.add_argument('--port',required=True , help='Enter serial port')
args = parser.parse_args()
#print("upTopic" + args.up + " " + args.down)
print(vars(args))
    
# If you want to use a specific client id, use
# mqttc = mqtt.Client("client-id")
# but note that the client id must be unique on the broker. Leaving the client
# id parameter empty will generate a random id for you.
mqttc = mqtt.Client()
mqttc.on_message = on_message
mqttc.on_connect = on_connect
mqttc.on_publish = on_publish
mqttc.on_subscribe = on_subscribe
# Uncomment to enable debug messages
mqttc.on_log = on_log
mqttc.connect("vpn.linkwave.co.uk", 1883, 60)
#mqttc.connect("test.mosquitto.org", 1883, 60)
#mqttc.connect("iot.eclipse.org", 1883, 60)


# connect to serial port
global ser
ser = serial.Serial()
ser.port = args.port    # "COM19"
ser.baudrate = 9600
#ser.parity = "PARITY_NONE"
ser.rtscts = False
ser.xonxoff = False
ser.timeout = 0.05 ## None maybe non blocking read?

try:
    ser.open()
except serial.SerialException as e:
    logger.info('Could not open serial port {}: {}\n'.format(ser.name, e))
    sys.exit(1)

# ser.write(bytes("open\r\n",'utf-8'))

mqttc.loop_start()  # mqtt thread start


def main():
    global upcount
    global args
    while(True):
        if(mqttConnected):
            payload = ser.read(100)   # read  chars or time out
            if(len(payload) > 0):
                logger.info("len: " + str(len(payload)))
                logger.info("publish class start: " + str(upcount))
                infot = mqttc.publish(args.up, payload, qos=0)
                infot.wait_for_publish() ## ok this blocks - and will lock for ever if the net goes down
                logger.info("publish class end")

            send = 0
            #time.sleep(1)

if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print('Interrupted')
        exitHandler()
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)
    