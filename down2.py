#!/usr/bin/python


# This shows a simple example of an MQTT subscriber.
import logging.handlers
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

FORMAT = '%(asctime)s : %(name)s : %(levelname)s : %(message)s'
logging.basicConfig(format=FORMAT)  ## logger writes to console

logger.info('down2.py started')


#import context  # Ensures paho is in PYTHONPATH
import paho.mqtt.client as mqtt


def on_connect(mqttc, obj, flags, rc):
    logger.info("connect: " + str(rc))


def on_message(mqttc, obj, msg):
    logger.info("msg:" + msg.topic + " " + str(msg.qos) + " " + str(msg.payload))


def on_publish(mqttc, obj, mid):
    logger.info("publish: " + str(mid))


def on_subscribe(mqttc, obj, mid, granted_qos):
    logger.info("Subscribed: " + str(mid) + " qos: " + str(granted_qos))


def on_log(mqttc, obj, level, string):
    logger.info(string)


# If you want to use a specific client id, use
# mqttc = mqtt.Client("client-id")
# but note that the client id must be unique on the broker. Leaving the client
# id parameter empty will generate a random id for you.
mqttc = mqtt.Client()
mqttc.on_message = on_message
mqttc.on_connect = on_connect
mqttc.on_publish = on_publish
mqttc.on_subscribe = on_subscribe
# Uncomment to enable debug messages
# mqttc.on_log = on_log
#mqttc.connect("vpn.linkwave.co.uk", 1883, 60)
#mqttc.connect("test.mosquitto.org", 1883, 60)
mqttc.connect("iot.eclipse.org", 1883, 60)

mqttc.subscribe("test/ab", 0)

mqttc.loop_forever()