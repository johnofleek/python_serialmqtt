import atexit
import time
import sys
import os

import logging.handlers
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

FORMAT = '%(asctime)s : %(name)s : %(levelname)s : %(message)s'
logging.basicConfig(format=FORMAT)  ## logger writes to console

logger.info('up2.py started')

#import context  # Ensures paho is in PYTHONPATH
import paho.mqtt.client as mqtt

payload = "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?"

payload = "Sed ut perspiciatis"
count = 0
mqttConnected = False

def on_connect(mqttc, obj, flags, rc):
    global mqttConnected
    mqttConnected = True
    logger.info("connect: " + str(rc))
    
def on_disconnect(mqttc, obj, flags, rc):
    global mqttConnected

    mqttConnected = False
    logger.info("disconnect: " + str(rc))

def on_message(mqttc, obj, msg):
    logger.info("msg:" + msg.topic + " " + str(msg.qos) + " " + str(msg.payload))



def on_publish(mqttc, obj, mid):
    global send
    global count
    #logger.info("publish: " + str(mid) + " count: " + str(count))
    #time.sleep(0.20)

    send = 1    # set send flag
    count+=1
    


def on_subscribe(mqttc, obj, mid, granted_qos):
    logger.info("Subscribed: " + str(mid) + "qos " + str(granted_qos))


def on_log(mqttc, obj, level, string):
    logger.info(string)


def exitHandler():
    global mqttc
    
    mqttc.loop_start()
    logger.info("mqtt threads stopped")
    
     
# If you want to use a specific client id, use
# mqttc = mqtt.Client("client-id")
# but note that the client id must be unique on the broker. Leaving the client
# id parameter empty will generate a random id for you.
mqttc = mqtt.Client()
mqttc.on_message = on_message
mqttc.on_connect = on_connect
mqttc.on_publish = on_publish
mqttc.on_subscribe = on_subscribe
# Uncomment to enable debug messages
mqttc.on_log = on_log
mqttc.connect("vpn.linkwave.co.uk", 1883, 60)
#mqttc.connect("test.mosquitto.org", 1883, 60)
#mqttc.connect("iot.eclipse.org", 1883, 60)

mqttc.loop_start()  # mqtt thread start

while (mqttConnected == False):
    x=1
    
#logger.info("publish start: this")
#(rc, mid) = mqttc.publish("test", "this", qos=1)
#logger.info("publish end: this")

logger.info("publish class start")
infot = mqttc.publish("test/ab", "startPublish", qos=0)
logger.info("publish class end")
send = 1

infot.wait_for_publish() ## ok this blocks - and will lock for ever if the net goes down
logger.info("publishing done")

def main():
    while(True):
        if(mqttConnected):
            logger.info("publish class start: " + str(count))
            infot = mqttc.publish("test/ab", payload, qos=0)
            infot.wait_for_publish() ## ok this blocks - and will lock for ever if the net goes down
            logger.info("publish class end")

            send = 0
            time.sleep(1)
    
    
if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print('Interrupted')
        exitHandler()
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)
            
            