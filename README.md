# python_serialMqtt

Project to investigate the performance of a python based serial port to mqtt bridge written in Python 3.7.2 

# Installation

## Step 1
Windows - installed python 3.7.2 from  
https://www.python.org/downloads/

## Step 2
locally install MQTT library - could have tried pip but I want to be able to transfer the app in one go

https://pypi.org/project/paho-mqtt/#files

# Current status
Two test apps are available   
**up2.py** transmits a payload of around 2k bytes continuously  
**down2.py** receives the payload  
Both scripts use the python logging function to command line to provide time-stamping  
Current performance indicates that latency is <50ms  


# Considerations

## Data usage
- Keep alive is needed if the device needs to be always connectible
- Keep alive will consume data
- Keep alive rate is configurable and will be a balance between network TCP socket closure and data usage  

TODO: investigate keep alive data usage - maybe use wireshark  

An optional workaround is to implement an SMS push to connect mechanism maybe with an in activity timeout  

## Benefits - easy device to device communication
End point devices are able to communicate with each other without fixed IP or dynamic DNS, Non NAPT. For example so long as the endpoint can connect out over IP to the INTERNET it will be possible for 2 endpoints to communicate with each other.  
* No special SIM contracts needed
* No port forwarding  
* No private networks  
* No VPN required   

# A network broker is needed
This will need some automation to configure device to device communication. Initially this just works but in a real system the broker will need configuring to stop eavsdropping or false data being injected


# next steps
* Add a serial port connection - up-link payload aggregation needs considering vs data packet packing efficiency
* Make a bidirectional serial port script




# git
git pull origin master
git clone https:/gitlab.com/johnofleek/python_serialmqtt


